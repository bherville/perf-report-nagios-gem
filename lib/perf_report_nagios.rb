require 'perf_report_nagios/version'

module PerfReportNagios
  class GraphSource
    @graph_command
    @graph_types
    @time_zone
    @logger

    def initialize(report_hash, logger = Rails.logger)
      @graph_command  = report_hash[:graph_command]
      @graph_types    = report_hash[:graph_types]
      @time_zone      = report_hash[:time_zone]

      @logger         = report_hash[:logger]
      @logger         ||= Rails.logger
    end

    def generate_graphs(tmp_graph_dir, servers, file_type = :png)
      server_graph = Hash.new
      @logger.info("Generating Graphs to: #{tmp_graph_dir}")

      Dir.mkdir tmp_graph_dir unless Dir.exists?(tmp_graph_dir)

      servers.each do |server|
        @logger.debug("Generating Graphs for: #{server.name}")
        server_graph[server.name] = Array.new

        server.graphs.each do |graph|
          next unless valid_graph?(graph)

          graph_path = graph.file_path

          output = `#{graph_path} #{@graph_command} -z "#{@time_zone}" -g #{tmp_graph_dir}`

          @logger.debug("Graph command output: #{output}")

          graph_file = "#{server.name}_#{graph.name}#{graph_extension(file_type)}"
          server_graph[server.name] << "#{File.join(tmp_graph_dir, graph_file)}"
        end
      end

      server_graph
    end

    private
    def valid_graph?(graph)
      valid = false

      @graph_types.each do |valid_graph|
        if graph.name == valid_graph.name
          valid = true
        end
      end

      valid
    end

    def graph_extension(file_type)
      case file_type
        when :png
          '.png'
        when :svg
          '.svg'
        else
          '.png'
      end
    end
  end
end
