module PerfReportNagios
  module Generators
    class InstallGenerator < ::Rails::Generators::Base
      source_root File.expand_path('../../templates', __FILE__)

      desc 'Install the Nagios PerfReport Module.'

      def copy_module_file
        template 'perf_report_nagios_initializer.rb', 'config/initializers/perf_report_nagios_initializer.rb'

        template 'module.yml', 'lib/graph_source_modules/nagios/module.yml'
      end
    end
  end
end