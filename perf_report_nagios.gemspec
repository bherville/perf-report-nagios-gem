# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'perf_report_nagios/version'

Gem::Specification.new do |spec|
  spec.name          = 'perf_report_nagios'
  spec.version       = PerfReportNagios::VERSION
  spec.authors       = ['Brad Herring']
  spec.email         = ['brad@bherville.com']

  spec.summary       = %q{Perf Report Nagios graph source gem}
  spec.description   = %q{Perf Report Nagios graph source gem}
  spec.homepage      = 'https://bitbucket.org/bherring/perf-report-nagios'
  spec.license       = 'GPL'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.8'
  spec.add_development_dependency 'rake', '~> 10.0'
end
